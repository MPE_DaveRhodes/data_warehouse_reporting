﻿using System.Collections.Generic;
using System.Web.Mvc;
using Snowflake_Connector;

namespace WarehouseReportingWebApp.Controllers
{
    [Authorize]
    public class SnowflakeController : Controller
    {
        /// <summary>
        /// Add user's claims to viewbag
        /// </summary>
        /// <returns></returns>
        public ActionResult SnowflakeIndex()
        {
            var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;

            ////You get the user’s first and last name below:
            ViewBag.Name = userClaims?.FindFirst("name")?.Value;

            //// The 'preferred_username' claim can be used for showing the username
            ViewBag.Username = userClaims?.FindFirst("preferred_username")?.Value;

            ViewBag.testResult = new List<string>();

            return View();
        }

        public ActionResult SnowflakeTest()
        {
            ViewBag.testResult = DataConnector.TestConnection();

            return View();
        }
    }
}
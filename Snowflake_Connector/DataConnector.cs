﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snowflake.Data.Client;

namespace Snowflake_Connector
{
    public static class DataConnector
    {
        private static IDbConnection conn = new SnowflakeDbConnection();

        static DataConnector()
        {
            string acc = System.Configuration.ConfigurationManager.AppSettings["SnowflakeAccount"];
            string user = System.Configuration.ConfigurationManager.AppSettings["SnowflakeUser"];
            string pw = System.Configuration.ConfigurationManager.AppSettings["SnowflakePassword"];
            string host = System.Configuration.ConfigurationManager.AppSettings["SnowflakeHost"];

            conn.ConnectionString = "account="+acc+";user="+user+";password="+pw+";host="+host+";";
        }

        public static List<string> TestConnection()
        {
            conn.Open();

            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = "select * from clean.analytics_finance.monthly_target_seed";
            IDataReader reader = cmd.ExecuteReader();

            List<string> outList = new List<string>();

            while (reader.Read())
            {
                Console.WriteLine(reader.GetString(0));
                outList.Add(reader.GetString(0));
            }

            conn.Close();

            return outList;
        }
    }
}
